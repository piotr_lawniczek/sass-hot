var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: {
		app: [
			'./src/js/main.js',
			'./src/sass/main.scss'
		]
	},
	output: {
		path: __dirname + '/dist/js',
		filename: 'all.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/
			},
			{
				test: /\.s[ac]ss$/,
				use: ExtractTextPlugin.extract({
					use: ['css-loader', 'sass-loader'],
					fallback: 'style-loader'
				})
			}

		]
	},
	plugins: [
		new ExtractTextPlugin('../css/style.css')
	]
}